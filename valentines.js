var anFrame = 0;
var interval;

function animate(){
	var frames = makeFrames();
	interval = setInterval(function(){
		frames[anFrame]();
		anFrame++;
		if(anFrame >= frames.length){
			clearInterval(interval);
		}
	
	}, 1000/24);
}

function makeFrames(){
	
	var frames = [];
	let emptyFrame = function(){};
	frames.push(function(){
		moveSky("100%", "150%", "200%");
	});
	frames.push(function(){
	});
	frames.push(function(){
		moveSky("90%", "140%", "190%");
	});
	frames.push(function(){});
	frames.push(function(){
		moveSky("80%", "130%", "180%");
	});
	frames.push(function(){});
	frames.push(function(){
		moveSky("70%", "120%", "170%");
		moveGround("98.6");
	});
	frames.push(function(){});
	frames.push(function(){
		moveSky("60%", "110%", "160%");
		moveBlueMountain("96");
		moveGround("96.2");
	});
	frames.push(function(){});
	frames.push(function(){
		moveSky("50%", "100%", "150%");
		moveBlueMountain("92");
		moveGround("94.8");
	});
	frames.push(function(){});
	frames.push(function(){
		movePurpleMountain("90");
		moveBlueMountain("88");
		moveGround("92.4");
		moveSky("40%", "90%", "140%");
	});
	frames.push(function(){});
	frames.push(function(){
		movePurpleMountain("80");
		moveBlueMountain("84");
		moveGround("91");
		moveSky("30%", "80%", "130%");
	});
	frames.push(function(){});
	frames.push(function(){
		movePurpleMountain("75");
		moveBlueMountain("80");
		moveGround("90.5");
		moveSky("20%", "70%", "120%");
	});
	frames.push(function(){});
	frames.push(function(){
		movePurpleMountain("70");
		moveBlueMountain("78");
		moveGround("90");
		moveSky("10%", "60%", "110%");
	});
	frames.push(function(){
		movePurpleMountain("69");
		moveBlueMountain("76");
	});
	frames.push(function(){
		movePurpleMountain("68");
		moveBlueMountain("75");
		moveSky("0%", "50%", "100%");
	});
	for(var i = 0; i < 9; i++){
		frames.push(emptyFrame);
	}
	frames.push(function(){
		moveMyFace(104, 50);
	});
	frames.push(function(){
		moveMyFace(103, 50);
	});
	frames.push(function(){
		moveMyFace(102, 50);
	});
	frames.push(function(){
		moveMyFace(101, 50);
		moveMyBody(102, 65);
	});
	frames.push(function(){
		moveMyFace(101, 50);
	});
	for(var i = 0; i < 9; i++){
		frames.push(emptyFrame);
	}
	frames.push(function(){
		moveYourFace(-4, 55);
	});
	frames.push(function(){
		moveYourFace(-3, 55);
	});
	frames.push(function(){
		moveYourFace(-2, 55);
	});
	frames.push(function(){
		moveYourFace(-1, 55);
	});
	for(var i = 0; i < 17; i++){
		frames.push(emptyFrame);
	}
	frames.push(function(){
		moveMyEyes(96, 49.2);
	});
	frames.push(function(){
		moveMyEyes(96, 49.5);
	});
	frames.push(function(){
		moveMyEyes(96, 49.8);
	});
	frames.push(function(){
		moveMyEyes(96, 50.1);
	});
	frames.push(function(){
		moveMyEyes(96, 50.3);
	});
	frames.push(function(){
		moveMyEyes(96.025, 50.5);
	});
	frames.push(function(){
		moveMyEyes(96.05, 50.5);
	});
	frames.push(function(){
		moveMyEyes(96.075, 50.5);
	});
	frames.push(function(){
		moveMyEyes(96.1, 50.5);
	});
	frames.push(function(){
		moveMyEyes(96.2, 50.5);
	});
	frames.push(function(){
		moveMyEyes(96.3, 50.5);
	});
	frames.push(function(){
		reshapeMyEyes(1, 0.9);
	});
	frames.push(function(){
		reshapeMyEyes(1, 0.8);
	});
	frames.push(function(){
		reshapeMyEyes(1, 0.7);
	});
	frames.push(function(){
		reshapeMyEyes(1, 0.6);
	});
	for(var i = 0; i < 12; i++){
		frames.push(emptyFrame);
	}
	frames.push(function(){
		reshapeMyEyes(1, 0.8);
	});
	frames.push(function(){
		reshapeMyEyes(1, 1.1);
	});
	frames.push(function(){
		reshapeMyEyes(1, 1.3);
	});
	frames.push(function(){
		reshapeMyEyes(1, 1.5);
	});
	frames.push(function(){
		reshapeMyEyes(1, 1.6);
	});
	frames.push(function(){
		reshapeMyEyes(1, 1.3);
	});
	frames.push(function(){
		reshapeMyEyes(1, 1.1);
	});
	frames.push(function(){
		reshapeMyEyes(1, 1);
	});
	frames.push(function(){
		moveMyFace(100, 50);
	});
	frames.push(function(){
		moveMyFace(99, 50);
	});
	frames.push(function(){
		moveMyFace(98, 50);
	});
	frames.push(function(){
		moveMyFace(97, 50);
		moveYourEyes(3.4, 54);
	});
	frames.push(function(){
		moveMyFace(95, 50);
		moveYourEyes(3.3, 54);
	});
	frames.push(function(){
		moveMyFace(93, 50);
		moveYourEyes(3.2, 54);
		reshapeYourEyes(1, 0.9);
	});
	frames.push(function(){
		moveMyFace(91, 49.5);
		reshapeYourEyes(1, 1.1);
	});
	frames.push(function(){
		moveMyFace(89, 49);
		reshapeYourEyes(1, 1.3);
	});
	frames.push(function(){
		moveMyFace(86, 49.5);
		reshapeYourEyes(1, 1.5);
		moveYourFace(0, 55);
	});
	frames.push(function(){
		moveMyFace(83, 50);
		moveYourFace(1, 55);
	});
	frames.push(function(){
		moveMyFace(80, 49.5);
		moveYourFace(2, 55);
	});
	frames.push(function(){
		moveMyFace(77, 49);
		reshapeYourEyes(1, 1.3);
		moveYourFace(3, 55);
	});
	frames.push(function(){
		moveMyFace(74, 49.5);
		reshapeYourEyes(1, 1.1);
		moveYourFace(5, 55);
	});
	frames.push(function(){
		moveMyFace(71, 50);
		reshapeYourEyes(1, 1);
		moveYourFace(8, 54);
	});
	frames.push(function(){
		moveMyFace(68, 49.5);
		moveYourFace(11, 54);
	});
	frames.push(function(){
		moveMyFace(65, 49);
		moveYourFace(14, 54);
	});
	frames.push(function(){
		moveMyFace(62, 49.5);
		moveYourFace(17, 54);
	});
	frames.push(function(){
		moveMyFace(59, 50);
		moveYourFace(14, 54);
	});
	frames.push(function(){
		moveMyFace(56, 49.5);
		moveYourFace(20, 54);
		moveYourEyes(24.2, 53.7);
	});
	frames.push(function(){
		moveMyFace(53, 49);
		moveYourFace(23, 54);
		moveYourEyes(27.2, 53.5);
	});
	frames.push(function(){
		moveMyFace(50, 49.5);
		moveYourFace(26, 54);
		moveYourEyes(30.2, 53.3);
	});
	frames.push(function(){
		moveMyFace(47, 50);
		moveYourFace(29, 54);
		moveYourEyes(33.2, 53.1);
	});
	frames.push(function(){
		moveMyFace(45, 49.5);
		moveMyEyes(41, 51.9);
		moveYourFace(31, 54);
		moveYourEyes(35.1, 52.9);
	});
	frames.push(function(){
		moveMyFace(43, 49);
		moveMyEyes(38.8, 52.1);
		moveYourFace(33, 54);
		moveYourEyes(36.9, 52.7);
	});
	frames.push(function(){
		moveMyFace(42, 49.5);
		moveMyEyes(38, 52.3);
		moveYourFace(34, 54.5);
		moveYourEyes(37.7, 52.5);
	});
	frames.push(function(){
		moveHeart(55, 0.1);
	});
	frames.push(function(){
		moveHeart(54, 0.2);
	});
	frames.push(function(){
		moveHeart(53, 0.3);
	});
	frames.push(function(){
		moveHeart(52, 0.4);
	});
	frames.push(function(){
		moveHeart(51, 0.5);
	});
	frames.push(function(){
		moveHeart(50, 0.6);
	});
	frames.push(function(){
		moveHeart(49, 0.7);
	});
	frames.push(function(){
		moveHeart(48, 0.8);
	});
	frames.push(function(){
		moveHeart(47, 0.9);
	});
	frames.push(function(){
		moveHeart(46, 1);
	});
	frames.push(function(){
		moveHeart(45, 1);
	});
	frames.push(function(){
		moveHeart(44, 1);
	});
	frames.push(function(){
		moveHeart(43, 1);
	});
	frames.push(function(){
		moveMyEyes(38.2, 52.3);
		moveYourEyes(37.7, 52.7);
	});
	frames.push(function(){
		moveMyEyes(38.4, 52.3);
		moveYourEyes(37.7, 52.9);
	});
	
	
	return frames;
}

function moveSky(purpleOffset, pinkOffset, vPinkOffset){
	var purple = document.querySelector("#purple");
	var pink = document.querySelector("#pink");
	var vPink = document.querySelector("#veryPink");
	vPink.setAttribute("offset", vPinkOffset);
	pink.setAttribute("offset", pinkOffset);
	purple.setAttribute("offset", purpleOffset);
}

function movePurpleMountain(height){
	let shape = " l 30 -5 l 15 7 l 30 -9 l 25 10 l 0 32 l -100 0 z";
	let ground = document.querySelector("#ground");
	ground.setAttribute("d", "M 0 "+ height+ shape);
}

function moveBlueMountain(height){
	let shape = " l 30 -6 l 15 5 l 15 -6 l 10 5 l 10 -2 l 20 5 l 0 26 l -100 0 z"
	let ground = document.querySelector("#ground2");
	ground.setAttribute("d", "M 0 "+ height + shape);
}

function moveGround(height){
	let shape = " l 100 2 l 0 8 l -100 0 z"
	let ground = document.querySelector("#ground3");
	ground.setAttribute("d", "M 0 "+ height + shape);
}

function moveMyFace(x, y){
	let mask = document.querySelector("#face1");
	mask.setAttribute("cx", x);
	mask.setAttribute("cy", y);
	let face = document.querySelector("#myFace");
	let eye = document.querySelector("#myEye");
	let differenceX = Number(face.getAttribute("cx")) - Number(eye.getAttribute("cx"));
	let differenceY = Number(face.getAttribute("cy")) - Number(eye.getAttribute("cy"));
	face.setAttribute("cx", x);
	face.setAttribute("cy", y);
	moveMyEyes(x-differenceX, y-differenceY);
	moveMyBody(x+1, y+17);
}

function moveMyBody(x, y){
	let body = document.querySelector("#myBody");
	body.setAttribute("cx", x);
	body.setAttribute("cy", y);
}

function moveMyEyes(x, y){
	let eye = document.querySelector("#myEye");
	eye.setAttribute("cx", x);
	eye.setAttribute("cy", y);
}

function moveYourFace(x, y){
	let mask = document.querySelector("#face2");
	mask.setAttribute("cx", x);
	mask.setAttribute("cy", y);
	let face = document.querySelector("#yourFace");
	let eye = document.querySelector("#yourEye");
	let differenceX = Number(face.getAttribute("cx")) - Number(eye.getAttribute("cx"));
	let differenceY = Number(face.getAttribute("cy")) - Number(eye.getAttribute("cy"));
	face.setAttribute("cx", x);
	face.setAttribute("cy", y);
	moveYourEyes(x-differenceX, y-differenceY);
	moveYourBody(x-.8, y + 15);

}

function moveYourBody(x, y){
	let body = document.querySelector("#yourBody");
	body.setAttribute("cx", x);
	body.setAttribute("cy", y);
}

function moveYourEyes(x, y){
	let eye = document.querySelector("#yourEye");
	eye.setAttribute("cx", x);
	eye.setAttribute("cy", y);
}

function reshapeMyEyes(rx, ry){
	let eye = document.querySelector("#myEye");
	eye.setAttribute("rx", rx);
	eye.setAttribute("ry", ry);
}

function reshapeYourEyes(rx, ry){
	let eye = document.querySelector("#yourEye");
	eye.setAttribute("rx", rx);
	eye.setAttribute("ry", ry);
}

function moveHeart(height, opacity){
	let shape = "q -1 -3 -5 -5 q 2.5 -5 5 0 q 2.5 -5 5 0 q -4 2 -5 5 z"
	let heart = document.querySelector("#heart");
	heart.setAttribute("d", "M 38 " + height + " q -1 -3 -5 -5 q 2.5 -5 5 0 q 2.5 -5 5 0 q -4 2 -5 5 z");
	heart.setAttribute("opacity", opacity);
}
